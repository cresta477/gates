<?php

namespace App\Providers;
use App\User;
use App\Policies\AdminPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        User::class => AdminPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        
        Gate::define('admin-panel', 'AdminPolicy@adminPanel');
        Gate::define('sales-access', 'AdminPolicy@salesMenu');
        // Gate::define('pharmacy-access', 'AdminPolicy@pharmacyMenu');

        // Gate::define('admin-panel', function ($user) {
        //    echo 'xxx';
        //    die();
        // });
    }
}
