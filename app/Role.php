<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
   public function users(){
   	return $this->belongsToMany('App\User');
   }

   public function menus(){
   	return $this->belongsToMany('App\Menu');
   }

   // public function hasAccessToMenu($menus){
   // 		if($this->hasAnyMenu($menus)){
   // 			return true;
   // 		}
   // 		return false;
   // }

   // public function hasAnyMeny
}
