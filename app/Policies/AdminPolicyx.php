<?php

namespace App\Policies;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function adminPanel($user){
        $menuName = $user['menu-title'];
        if($user->has('roles')){
           foreach($user->roles as $role){
                $roleId = $role->id;
           }

           $role = Role::find($roleId);


           foreach($role->menus as $menu){
                if($menu->name == $menuName){
                    return true;
                }
           }
        }
        return false;
        
    }
}
