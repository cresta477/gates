<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pharmacy', function () {
	if(Gate::allows('pharmacy', Auth::user())){
		return 'pharmacy department';
	}else{
		return 'not authorized to view this page';
	}
})->name('pharmacy');

Route::get('/admin', function () {
	if(Gate::allows('admin-panel', Auth::user())){
		return 'admin panel';
	}else{
		return 'not authorized to view this page';
	}
})->name('admin');

Route::get('/sales', function () {
	if(Gate::allows('sales-access', Auth::user())){
		return 'sales department';
	}else{
		return 'not authorized to view this page';
	}
})->name('sales');


