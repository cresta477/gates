<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/pharmacy', function () {
	$user = Auth::user();
	$user['menu-title'] = 'pharmacy';
	if(Gate::allows('admin-panel', $user)){
    	return 'pharmacy department';
	}else{
		return 'not authorized to view this page';
	}
})->name('pharmacy');

Route::get('/sales', function () {
	$user = Auth::user();
	$user['menu-title'] = 'sales';
	if(Gate::allows('admin-panel', $user)){
    	return 'sales department';
    }else{
    	return 'not authorized to view this page';
    }
})->name('sales');


Route::get('/admin', function () {
	$user = Auth::user();
	$user['menu-title'] = 'admin';
	
	if(Gate::allows('admin-panel', $user)){
		return 'admin panel';
	}else{
		return 'not authorized to view this page';
	}
    
})->name('admin');