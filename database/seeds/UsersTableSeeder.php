<?php

use Illuminate\Database\Seeder;
use App\Role;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin_role = Role::where('name', 'admin')->first();
        $pharmacy_role = Role::where('name', 'pharmacy')->first();
        $sales_role = Role::where('name', 'sales')->first();

        $user1 = new User();
       	$user1->name = 'bikram';
	    $user1->email = 'bikram@gmail.com';
	    $user1->password = bcrypt('secret');
	    $user1->created_by = 1;
	    $user1->save();
	    $user1->roles()->attach($admin_role);

	    $user2 = new User();
       	$user2->name = 'cresta';
	    $user2->email = 'cresta@gmail.com';
	    $user2->password = bcrypt('secret');
	    $user2->created_by = 1;
	    $user2->save();
	    $user2->roles()->attach($pharmacy_role);

	    $user3 = new User();
       	$user3->name = 'power';
	    $user3->email = 'power@gmail.com';
	    $user3->password = bcrypt('secret');
	    $user3->created_by = 1;
	    $user3->save();
	    $user3->roles()->attach($sales_role);


    }
}
