<?php

use Illuminate\Database\Seeder;
use App\Menu;
class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminMenu = new Menu();
        $adminMenu->name('admin');
        $adminMenu->save();

        $pharmacyMenu = new Menu();
        $pharmacyMenu->name('pharmacy');
        $pharmacyMenu->save();

        $salesMenu = new Menu();
        $salesMenu->name('sales');
        $salesMenu->save();
    }
}
