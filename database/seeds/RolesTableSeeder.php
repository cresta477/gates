<?php

use Illuminate\Database\Seeder;
use App\Role;
class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $admin_role = new Role();
       $admin_role->name = 'admin';
       $admin_role->save();

       $pharmacy_role = new Role();
       $pharmacy_role->name = 'pharmacy';
       $pharmacy_role->save();

       $sales_role = new Role();
       $sales_role->name = 'sales';
       $sales_role->save();
    }
}
